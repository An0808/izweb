<?php
    require_once '../config/Database.php';
    class Users extends  Database {
        // get data tabale Categories
      
        public function updateUsers($e,$a){
            $sql="UPDATE users SET actve = null  WHERE email = '$e' and actve ='$a' LIMIT 1";  
            echo $sql;
            return self::execute($sql);
        }

        public function getAllDataUser($mang){
            return parent::getAllData('users',$mang);

        }

        public function  getDataUserId($id){
            return parent::getDataId('users','user_id',$id);
        }

        public function deleteUser($id){
            return parent::deleteData('users','user_id',$id);
        }

        public function editDataUser($fn,$ln,$e,$ul,$uid){
            //kiểm tra xem mail đã có trong csdl chưa
             $sql = "SELECT user_id FROM users WHERE email = ? AND user_id != ?";
            if($stmt =self::$conn->prepare($sql)){
                //gán tham sô 
                global $message;
                $stmt->bind_param('si',$e,$uid);
                  // Cho chay cau lenh prepare
                  $stmt->execute();

                // Luu lai ket qua cua cau lenh prepare
                  $stmt->store_result();
                if(mysqli_stmt_num_rows($stmt) == 0) {
                    // Email available, run query de update csdl
                    $query = "UPDATE users SET 
                                first_name = ?, 
                                last_name = ?, email = ?, 
                                user_level =? 
                                WHERE user_id = ? LIMIT 1";
                    if($upd_stmt = self::$conn->prepare($query)) {

                        // Gan tham so
                        $upd_stmt->bind_param('sssii', $fn, $ln, $e, $ul, $uid);

                        // Cho chay cau lenh
                        $upd_stmt->execute() or die("Mysqli Error: $query ". mysqli_stmt_error($upd_stmt));

                        if( $upd_stmt->affected_rows() == 1) {
                            $message = "<p class='success'>Ok ngon rồi</p>";
                        } else {
                            $message = "<p class='error'>No hỏng rồi .</p>";
                        }
                    }
                } else {
                    $message = "<p class='error'>Lỗi rồi trùng lặp</p>";
                }
            }
        }

    }
?>