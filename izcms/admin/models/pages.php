<?php
    require_once '../config/Database.php';
    class Pages extends  Database {
        // public function allCategories(){
        //     return parent::getAllData('page','position');
        // }   
        public function addPage($mang){
            return parent::addData('pages',$mang);
        }
        public function countPages(){
            return parent::count('pages','page_id');
        }

        public function allPagesJoin($order_by){
            $result = self::execute(" SELECT p.page_id, p.page_name, DATE_FORMAT(p.post_on,'%b %d %y') AS date, CONCAT_WS(' ',first_name,last_name) AS name,p.content as content FROM pages as p JOIN users AS u USING (user_id) ORDER BY $order_by ASC ");
            if(mysqli_num_rows($result) >0){
                while($row=mysqli_fetch_object($result)){
                    $data[]=$row;
                }
            }
            else{
                $data=array();
            }
            return $data;
        }

        public function allPageId($id){
            return parent::getDataId('pages','page_id',$id);
        }

        public function updatePage($mang,$id){
            return parent::updateData('pages',$mang,'page_id',$id);

        }
        public function deletePage($id){
            return parent::deleteData('pages','page_id',$id);
        }

    }
?>