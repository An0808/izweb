<div id="content">
<h2>Manage Users</h2>
    <table>
<thead>
	<tr>
		<th><a href="index.php?controller=manage-user&action=view&sort=fn">First Name</a></th>
		<th><a href="index.php?controller=manage-user&action=view&sort=ln">Last Name</a></th>
		<th><a href="index.php?controller=manage-user&action=view&sort=e">Email</a></th>
        <th><a href="index.php?controller=manage-user&action=view&sort=lv">User Level</a></th>
        <th>Edit User</th>
        <th>Delete User</th>
	</tr>
</thead>
<tbody>
    <?php 
          //  echo '<pre>';
          //  print_r($categoriesJoin);
          foreach($users as $user){
       ?>
       <tr>
           <td><?php echo $user ->first_name?></td>
           <td><?php echo $user ->last_name ?></td>
           <td><?php echo $user ->email ?></td>
           <td><?php echo $user ->user_level ?></td>
           <td><a class='edit' href='index.php?controller=manage-user&action=edit&uid=<?php echo $user->user_id ?>'>Edit</a></td>
           <td><a class='delete'  onclick="return confirm('Are you sure you want to delete?');" href='index.php?controller=manage-user&action=delete&uid=<?php echo $user->user_id ?>'>Delete</a></td>
       </tr>
              

      <?php      
          }
      ?>
   </tbody>
</table>
</div>