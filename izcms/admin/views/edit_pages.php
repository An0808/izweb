<div id="content">
    <h2>Edit page : <?php echo $pageId->page_name?></h2>
    <?php 
         //print_r($pageId);
         print_r($categoriesId);
         echo $categoriesId->cat_name;
        if(isset($messages)){
            echo $messages;
        }
    ?>
 
 <form id="login" action="" method="post">
            <fieldset>
            	<legend>Add a Page</legend>
                    <div>
                        <label for="page">Page Name: <span class="required">*</span></label>
                            <?php if(isset($errors) && in_array('page_name',$errors)){
                                echo '<p style="color:red">page_name is fields</p>';
                            } ?>
                        <input type="text" name="page_name" id="page_name" value="<?php if(isset($_POST['page_name'])) echo strip_tags($_POST['page_name']); else{echo $pageId->page_name;} ?>" size="20" maxlength="80" tabindex="1" />
                        
                    </div>
                    <div>
                        <label for="category">All categories: <span class="required">*</span></label>

                            <?php if(isset($errors) && in_array('category',$errors)){
                                echo '<p style="color:red">category is fields</p>';
                            } ?>
                        <select name="category">
                            <option>Select Category</option>

                            <?php 
                                foreach($categories as $value){
                                    if( strcasecmp($value->cat_name,$categoriesId->cat_name) == 0){
                                        $a='selected= \"selected\"';
                                    }
                                    else{
                                        $a ='';
                                    }
                                    echo "<option $a value=\"$value->car_id\">$value->cat_name</option>";
                                }
                            ?>

                        </select>
                    </div>
                    <div>
                        <label for="position">Position: <span class="required">*</span></label>
                        <?php if(isset($errors) && in_array('position',$errors)){
                                echo '<p style="color:red">position is fields</p>';
                            } ?>
                        <select name="position">
                            <option>Select position</option>

                            <!-- position php -->
                            <?php for($i=1 ; $i<=$num+1;$i++ ){
                                if(isset($_POST['position']) && $_POST['position']==$i){
                                    $a='selected= \"selected\"';
                                }
                                elseif($i==$pageId->position){
                                    $a='selected= \"selected\"';
                                }
                                else{
                                    $a='';
                                }
                                echo "<option $a value=\"$i\">$i</option>";
                            } ?> 

                        </select>
                    </div>                
                    <div>
                        <label for="page-content">Page Content: <span class="required">*</span> </label>

                        <?php if(isset($errors) && in_array('content',$errors)){
                                echo '<p style="color:red">content is fields</p>';
                            } ?>

                        <textarea name="content" cols="50" rows="20"><?php if(isset($_POST['content'])) echo strip_tags($_POST['content']); else{echo $pageId->content;} ?></textarea>
                    </div>
            </fieldset>
            <p><input type="submit" name="submit" value="Add Page" /></p>
        </form>

</div>