<div id="content">
    <h2>Create a Categories</h2>
    <?php 
        if(isset($messages)){
            echo $messages;
        }
    
    ?>
    
    <form id="add_cat" action="" method="post">
        <fieldset>
            <legend>Add Category</legend>
            <div>
                <label for="category">Category Name :<span class ="required">*</span></label>
                <input type="text" name="category" id="category" value="<?php if(isset($_POST['category']))echo strip_tags($_POST['category']); else echo $categories->cat_name ?>" size="20" maxlenght="150" tabindex="1" />
                <?php if(isset($errors) && in_array('category',$errors)){
                    echo '<p style="color:red">category is fields</p>';
                } ?>
            </div>
            <div>
                <?php if(isset($errors) && in_array('position',$errors)){
                    echo '<p/>Please pick a  position<p/>';
                } ?>
                <label for="position"> Position: <span class ="required">*</span></label>
                <select name="position" tabindex="2">
                    <?php for($i=1 ; $i<=$num+1;$i++ ){
                        if(isset($_POST['position']) && $_POST['position']==$i){
                            $a='selected= \"selected\"';
                        }
                        elseif($i==$categories->position){
                            $a='selected= \"selected\"';
                        }
                        else{
                            $a='';
                        }
                        echo "<option $a value=\"$i\">$i</option>";
                    } ?>        
                </select>

            </div>
        </fieldset>
        
        <p><input  type="submit" name="add" valua="Add Category"></input></p>
    </form>

</div>