<?php   
    include 'models/user.php';
    if(isset($_GET['action'])){
        $action=$_GET['action'];
    }
    else{
        $action = null;
        redirect_to_admin();
    }
    switch($action){
        case 'view':
            if(isset($_GET['sort'])){
                $sort=$_GET['sort'];
            }
            else{
                $sort = null;
            }
            switch($sort){
                case 'fn':
                    $order_by = 'first_name';
                    break;
                case 'ln':
                    $order_by = 'last_name';
                    break;
                case 'lv':
                    $order_by ='user_level';
                    break;
                case 'e':
                    $order_by ='email';
                    break;
                    
                default:
                    $order_by='user_level';
                    break;
            }
            $users=Users::getAllDataUser($order_by);
            // print_r($users);
            require_once 'views/manage_page.php';
            break;
        case 'edit':
            
            if(isset($_GET['uid']) && filter_var($_GET['uid'],FILTER_VALIDATE_INT, array('min_range' => 1))){
                $uid=$_GET['uid']; 
                $user=Users::getDataUserId($uid);

                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $errors = array();
                    // Trim all incoming data
                    $trimmed = array_map('trim', $_POST);
                    
                    if(preg_match('/^[\w]{2,10}$/i', $trimmed['first_name'])) {
                        $fn = $trimmed['first_name'];
                        } else {
                        $errors[] = "first_name";
                    }
        
                    if(preg_match('/^[\w ]{2,10}$/i', $trimmed['last_name'])) {
                        $ln = $trimmed['last_name'];
                        } else {
                        $errors[] = "last name";
                    }
        
                    if(filter_var($trimmed['email'],FILTER_VALIDATE_EMAIL)) {
                        $e = $trimmed['email'];
                        } else {
                        $errors[] = "email";
                    }
                        
                    if(filter_var($trimmed['user_level'], FILTER_VALIDATE_INT, array('min_range'=>1))) {
                        $ul = $trimmed['user_level'];
                        } else {
                        $errors[] = "user level";
                    }
                    
                    
                    if(empty($errors)) {
                     Users::editDataUser($fn,$ln,$e,$ul,$uid);
                    }
                }

            }
            if($user == 0){
                redirect_to_admin('admin/index.php?controller=manage-user&action=view');
            }
            require_once 'views/edit_user.php';
            break;   
        case 'delete':
            if(isset($_GET['uid']) &&  filter_var($_GET['uid'],FILTER_VALIDATE_INT,array('min_renge'=>1)) ){
                $id = $_GET['uid'];
                if(Users::deleteUser($id))
                    {
                        $messages='Đã xoá thành công!';
                        redirect_to_admin('admin/index.php?controller=manage-user&action=view');
                    }
                    else {
                        $messages='Lỗi';
                    }
          
            break;  
            }
    }
?>