<?php
    require_once 'models/pages.php';
    require_once 'models/categories.php';
    require_once '../function/function.php';
    if(isset($_GET['action'])){
        $action=$_GET['action'];
    }
    else{
        $action = null;
    }

    switch($action){

        case 'view':
            // sort 
            if(isset($_GET['sort'])){
                $sort=$_GET['sort'];
            }
            else{
                $sort = null;
            }
            switch($sort){
                case 'cat':
                    $order_by = 'page_name';
                    break;
                case 'on':
                    $order_by = 'date';
                    break;
                case 'by':
                    $order_by ='name';
                    break;
                    
                default:
                    $order_by='date';
                    break;
            }
            $pagesJoin=Pages::allPagesJoin($order_by);
            require_once 'views/viewPage.php';
            break;

        // case 'list':
        //     $categories = categories::allCategories();
        //     require_once '../includes/siderbar-a.php';
        //     break;
        case 'add':
            
            // đêm col dung làm position
            $num = Pages::countPages();
            // get data bảng categries
            $categories = categories::allCategories();
            if($_SERVER['REQUEST_METHOD']=='POST'){
                //check errors
                $errors=  array();
                //page_name

                if(empty($_POST['page_name'])){
                    $errors[]= 'page_name';
                }
                else{
                    $page_name=$_POST['page_name'];
                }

                //category 

                if(isset($_POST['category']) && filter_var($_POST['category'],FILTER_VALIDATE_INT,array('min_renge'=>1))){
                    $car_id =$_POST['category'];
                }
                else{
                    $errors[]='category';
                }

                //position

                if(isset($_POST['position']) && filter_var($_POST['position'],FILTER_VALIDATE_INT,array('min_renge'=>1))){
                    $position =$_POST['position'];
                }
                else{
                    $errors[]='position';
                }

                // content

                if(empty($_POST['content'])){
                    $errors[]= 'content';
                }
                else{
                    $content=$_POST['content'];
                }

                ///user_id
              // echo $_POST['date'];
            //    $post_on = $_POST['date'];
            //    echo $post_on;
               $user_id = 1;

               // add page 
               if(empty($errors)){

                //neu ko co loi say ra thi thuc hien chuy van dữ liệu với hàm addCategories bên Module với mảng chuyền vòa là các giá trị 
                    if(Pages::addPage(['user_id'=>$user_id,'page_name'=>$page_name,'car_id'=> $car_id,'position'=>$position,'content'=>$content,'post_on'=>'now()']))
                    {
                        $messages =  '<p class="success">The Pages was added successfully!</p>';
                    }
                    else{
                        $messages = '<p class="warning">Could not added to the database due to a system erro.</p>';
                    }
               }
               else{
                  $messages = '<p class="warning">Plaese fill all the required fields</p>';
               } 
            }
            require_once 'views/add_pages.php';
            break;
        case 'edit':

            if(isset($_GET['id']) && filter_var($_GET['id'],FILTER_VALIDATE_INT,array('min_renge'=>1))){
                $id=$_GET['id'];

                if($_SERVER['REQUEST_METHOD']=='POST'){
                    //check errors
                    $errors=  array();
                    //page_name
    
                    if(empty($_POST['page_name'])){
                        $errors[]= 'page_name';
                    }
                    else{
                        $page_name=$_POST['page_name'];
                    }
    
                    //category 
    
                    if(isset($_POST['category']) && filter_var($_POST['category'],FILTER_VALIDATE_INT,array('min_renge'=>1))){
                        $car_id =$_POST['category'];
                    }
                    else{
                        $errors[]='category';
                    }
    
                    //position
    
                    if(isset($_POST['position']) && filter_var($_POST['position'],FILTER_VALIDATE_INT,array('min_renge'=>1))){
                        $position =$_POST['position'];
                    }
                    else{
                        $errors[]='position';
                    }
    
                    // content
    
                    if(empty($_POST['content'])){
                        $errors[]= 'content';
                    }
                    else{
                        $content=$_POST['content'];
                    }
                    
                  
                    ///user_id
                  // echo $_POST['date'];
                //    $post_on = $_POST['date'];
                //    echo $post_on;
                   $user_id = 1;
    
                   // add page 
                   if(empty($errors)){
                    //neu ko co loi say ra thi thuc hien chuy van dữ liệu với hàm addCategories bên Module với mảng chuyền vòa là các giá trị 
                        if(Pages::updatePage(['user_id'=>$user_id,'page_name'=>$page_name,'car_id'=> $car_id,'position'=>$position,'content'=>$content,'post_on'=>'now()'],$id))
                        {
                            $messages =  '<p class="success">The Pages was Edit successfully!</p>';
                        }
                        else{
                            $messages = '<p class="warning">Could not Edit to the database due to a system erro.</p>';
                        }
                   }
                   else{
                      $messages = '<p class="warning">Plaese fill all the required fields</p>';
                   } 
                }
            }
            // echo $id;
            $num = Pages::countPages();
            $categories = categories::allCategories();
            $pageId=Pages::allPageId($id);
            $categoriesId=Categories::addCategoriesId($pageId->car_id);
            require_once 'views/edit_pages.php';
            break;

        case 'delete':
            if(isset($_GET['id']) &&  filter_var($_GET['id'],FILTER_VALIDATE_INT,array('min_renge'=>1)) ){
                $id = $_GET['id'];
                if(Pages::deletePage($id))
                    {
                        $messages='Đã xoá thành công!';
                        header('Location:index.php?controller=pages&action=view');
                    }
                    else {
                        $messages='Lỗi';
                    }
            
            break;  
            }
    }
    
?>