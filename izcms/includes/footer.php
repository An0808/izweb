<div id="footer">
    <ul class="footer-links">
    <?php 
       
       if(isset($_SESSION['user_level'])) {
           // Neu co SESSION
           switch($_SESSION['user_level']) {
               case 0: // Registered users access
               echo "
                   <li><a href='index.php?controller=edit_profile'>User Profile</a></li>
                   <li><a href='index.php?controller=change_password'>Change Password</a></li>
                   <li><a href='#'>Personal Message</a></li>
                   <li><a href='index.php?controller=logout'>Log Out</a></li>
               ";
               break;
               
               case 2: // Admin access
               echo "
                   <li><a href='index.php?controller=edit_profile'>User Profile</a></li>
                   <li><a href='index.php?controller=change_password'>Change Password</a></li>
                   <li><a href='#'>Personal Message</a></li>
                   <li><a href='admin/index.php'>Admin CP</a></li>
                   <li><a href='index.php?controller=logout'>Log Out</a></li>
               ";
               break;
               
               default:
               echo "
                   <li><a href='".BASE_URL."register.php'>Register</a></li>
                   <li><a href='".BASE_URL."index.php?controller=login'>Login</a></li>
               ";
               break;
               
           }
           
       } else {
           // Neu khong co $_SESSION
           echo "
                   <li><a href='index.php'>Home</a></li>
                   <li><a href='index.php?controller=register'>Register</a></li>
                   <li><a href='index.php?controller=login'>Login</a></li>
               ";
       }
      ?>
    </ul>
</div><!--end footer-->