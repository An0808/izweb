<?php
    define('BASE_URL', 'http://izcms.test/');
    function redirect_to($page = 'index.php'){
        $url = BASE_URL.$page;
        header("Location: $url");
        exit(); 
    }
    function redirect($page){
        $url = BASE_URL.$page;
        header("Location: $url");
        exit(); 
    }
    //Hàm kiểm tra admin
    function is_admin(){
      return isset( $_SESSION['user_level']) && ($_SESSION['user_level']==2);
    }
    //kiểm tra quyn adin 
    function admin_access(){
        if(!is_admin()){
            redirect_to();
        }
    }
    function redirect_to_admin($page = 'admin/index.php'){
        $url =BASE_URL.$page;
        header("Location: $url");
        exit(); 
    }
    function is_loged_in(){
        if(!isset($_SESSION['user_id'])){
            redirect_to('?controller=login');
        }

    }

    function report_error($mgs){
        if(!empty($mgs)){
            foreach($mgs as $m){
                echo $m;
            }
        }
    }
        

    function the_excerpt($text){
        if(strlen($text) >200 ){
            $cutString = substr($text,0,200);
            $words =substr($text,0,strrpos($cutString,' '));
            return $words;
        }
        else{
            return $text;
        }
    }

    //Tao paragrap từ CSDL 
    function the_content($text){
        $sanitized= htmlentities($text,ENT_COMPAT,'UTF-8');
        return str_replace(array("\r\n","\n"),array("<p>","<p/>"),$sanitized);
    }

    // check capcha

    function captcha(){
        $qna = array(
            1 => array('question' => 'Mot cong mot', 'answer' => 2),
            2 => array('question' => 'ba tru hai', 'answer' => 1),
            3 => array('question' => 'ba nhan nam', 'answer' => 15),
            4 => array('question' => 'sau chia hai', 'answer' => 3),
            5 => array('question' => 'nang bach tuyet va .... chu lun', 'answer' => 7),
            6 => array('question' => 'Alibaba va ... ten cuop', 'answer' => 40),
            7 => array('question' => 'an mot qua khe, tra .... cuc vang', 'answer' => 1),
            8 => array('question' => 'may tui .... gang, mang di ma dung', 'answer' => 3)
            );
        $rand_key =array_rand($qna); // lấy ngẫu nhiêu 1 array 
        $_SESSION['capc']=$qna[$rand_key];
        return $question =$qna[$rand_key]['question'];
    }

    function pagination($page,$start,$display,$atid){
        $output .= "<ul class='pagination'>";
        if( $page > 1){
    
            //echo $start;
            //echo $display;
            $current_page =($start/$display) + 1;
    
            // echo $current_page;
            //nếu ko phải trangg đầu thì hiển thị trang trươc 
            if($current_page != 1){
                $output .=  "<li><a href='index.php?controller=author&atid=$atid&s=".$start=$display."&p=$page'>Previcus</a></li>";
            }
    
            //Hiển thị nhưng phần số còn lại của trang 
    
            for($i=1;$i<=$page;$i++){
                if($i !=$current_page){
                    $output .=   "<li><a href='index.php?controller=author&atid=$atid&s=".($display*($i -1))."&p=$page'>{$i}</a><li>";
                }
                else{
                    $output .=  "<li class=current>$i</li>";
                }
    
            }
            //end loop
    
            //nếu ko  phảu trng cuối thì hiển thj trang kề 
            if($current_page !=$page){
                $output .=  "<li><a href='index.php?controller=author&atid=$atid&s=".($start +$display)."&p=$page'>Next</a></li>";
            }
    
        }
        $output .=  "<ul/>";
         return $output;
    }

     function current_page($controlerr ,$action)
        {
            if($_GET['controller']==$controlerr && $_GET['action']==$action){
                echo "class='here'";
            }
        }
    function titile_page(){
        if(isset($_GET['controller'])&& isset($_GET['action'])){
            echo $_GET['controller']." ".$_GET['action'];
        }
        elseif(isset($_GET['controller'])){
            echo $_GET['controller'];
        }
        else {
            echo "Trang Chinh";
        }

    }