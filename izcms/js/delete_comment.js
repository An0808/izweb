$(document).ready(function(){
    $('.remove').click(function(){
        // lấy cái bao quang là container-wap
        var container = $(this).parent();
        // lấy giá trị từ id  thông qua hàm attr
        var cid =$(this).attr('id');
        // dữ liệu muons chuyên sang 
        var string = 'cmt_id='+cid;

        $.ajax({
            type : 'POST',
            url :'delete_comment.php',
            data :string,
            success: function(){
                container.slideUp('slow',function(){
                    container.remove();
                });
            }

        });
    });
});