<?php
    require_once 'config/Database.php';
    class Pages extends  Database {

        public function allPages(){
            return parent::getAllData('pages','position');
        }   
        public function allPageCategory(){
            return parent::getDataQuery('pages','categories','car_id','position');
        }
        public function addPage($mang){
            return parent::addData('pages',$mang);
        }
        public function countPages(){
            return parent::count('pages','page_id');
        }

        public function getDataIdJoin($sql,$id_table,$id,$start,$display){
            $result = self::execute(" SELECT p.page_name, p.page_id, $sql as content,
            DATE_FORMAT(p.post_on,'%b %d %Y') as date,
            CONCAT_WS(' ',u.first_name,u.last_name) AS name ,u.user_id
            FROM pages as p 
            INNER JOIN users as u 
            USING(user_id)
            WHERE p.$id_table = $id
            ORDER BY date ASC LIMIT $start,$display 
             ");
            if(mysqli_num_rows($result) >0){
                while($row=mysqli_fetch_object($result)){
                    $data[]=$row;
                }
            }
            else{
                $data=array();
            }
            return $data;
        }

        public function addCommentPage($mang){
            return parent::addData('comments',$mang);
        }

        public function getDataCommentPage($id){
            $result = self::execute("SELECT comment_id,author, comment, DATE_FORMAT(comment_date,'%b %d %Y') as date FROM comments WHERE page_id = $id");
               if( mysqli_num_rows($result) > 0)
               {
                    while($row = mysqli_fetch_object($result)){
                        $data[]=$row;
                    }               
               } 
               else{
                   $data =array();
               }
               return $data;
        }
        
        public function getCommentPageUser($id){
            $result = self::execute("SELECT p.page_name, p.content , 
                                    DATE_FORMAT(p.post_on ,'%b %d, %Y') AS date ,
                                    CONCAT_WS(' ',u.first_name,u.last_name) AS name ,
                                    u.user_id, COUNT(c.comment_id) AS count 
                                    FROM users as u INNER JOIN pages as p USING(user_id) 
                                    LEFT JOIN comments as c ON p.page_id=c.page_id WHERE p.page_id =$id
                                    GROUP BY p.page_name ORDER BY date ASC");
               
               if( mysqli_num_rows($result) > 0)
                {
                    while($row = mysqli_fetch_object($result)){
                        $data=$row;
                    }               
                } 
                else{
                    $data =null;
                }
                return $data;
        }

    }
?>