<?php
    require_once 'config/Database.php';
    class Categories extends  Database {
        public function allCategories(){
            return parent::getAllData('categories','position');
        }
        
        public function addCategories($mang){
            return parent::addData('categories',$mang);
        }

        public function countCategories(){
            return parent::count('categories','car_id');
        }

    }
?>