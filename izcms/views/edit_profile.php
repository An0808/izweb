<div id="content">
    <?php 
    print_r($dataUsers);
    echo $dataUsers->avatar;
    if(!empty($error)) echo $error;
    if(!empty($message)) echo $message;  ?> 
<h2>User Profile</h2>

<!-- form hiển thị  -->
<form enctype="multipart/form-data" action="" method="post"> 
    <fieldset>
		<legend>Avatar</legend>
		<div>
            <img class="avatar" src="upload/image/<?php echo (is_null($dataUsers->avatar) ? "no_image.jpg" :$dataUsers->avatar ); ?>" alt="avatar" />
            <p>Please select a JPEG or PNG image of 512Kb or smaller to use as avatar<p>
            </label> 
            <input type="hidden" name="MAX_FILE_SIZE" value="524288" />
            <input type="file" name="image" />
            <p><input class="change" type="submit" name="upload" value="Save changes" /></p>
        </div>
  </fieldset> 
</form>

<form action="" method="post">        
    <fieldset>
        <legend>User Info</legend>
        <div>
            <label for="first-name">First Name
                <?php if(isset($errors) && in_array('first_name',$errors)) echo "<p class='warning'>Please enter your first name.</p>";?>
            </label> 
            <input type="text" name="first_name" value="<?php if(isset($dataUsers->first_name)) echo strip_tags($dataUsers->first_name); ?>" size="20" maxlength="40" tabindex='1' />
        </div>
        
        <div>
            <label for="last-name">Last Name
                <?php if(isset($errors) && in_array('last name',$errors)) echo "<p class='warning'>Please enter your last name.</p>";?>
            </label> 
            <input type="text" name="last_name" value="<?php if(isset( $dataUsers->last_name)) echo strip_tags($dataUsers->last_name); ?>" size="20" maxlength="40" tabindex='1' />
        </div>
  </fieldset>
  <fieldset>
        <legend>Contact Info</legend>
        <div>
            <label for="email">Email
            <?php if(isset($errors) && in_array('email',$errors)) echo "<p class='warning'>Please enter a valid email.</p>";?>
            </label> 
            <input type="text" name="email" value="<?php if(isset($dataUsers->email)) echo $dataUsers->email; ?>" size="20" maxlength="40" tabindex='3' />
        </div>
        
        <div>
            <label for="website">Website</label> 
            <input type="text" name="website" value="<?php echo (is_null($user['website'])) ? '' : strip_tags($user['website']); ?>" size="20" maxlength="40" tabindex='4' />
        </div>
        
        <div>
            <label for="yahoo">Yahoo Messenger</label> 
            <input type="text" name="yahoo" value="<?php echo (is_null($dataUsers->yahoo)) ? '' : strip_tags($dataUsers->yahoo); ?>" size="20" maxlength="40" tabindex='5' />
        </div>
  </fieldset> 
  <fieldset>
        <legend>About Yourself</legend>
        <div>
            <textarea cols="50" rows="20" name="bio"><?php echo (is_null($dataUsers->bio)) ? '' : htmlentities($dataUsers->bio, ENT_COMPAT, 'UTF-8'); ?></textarea>
        </div>
  </fieldset>   
 <div><input type="submit" name="edit-user" value="Save Changes" /></div>
</form>

</div>