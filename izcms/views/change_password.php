<div id ="content">
    <h2>Change Password</h2>
    <?php if(isset($message)) echo $message; if(isset($errors)) {report_error($errors);}?>
    <form action="" method="post">          
        <fieldset>
            <legend>Change Password</legend>
            <div>
                <label for="Current Password">Current Password</label> 
                <input type="password" name="cur_password" value="" size="20" maxlength="40" tabindex='1' />
            </div>

            <div>
                <label for="New Password">New Password</label> 
                <input type="password" name="password1" value="" size="20" maxlength="40" tabindex='2' />
            </div>
            
            <div>
                <label for="Confirm Password">Confirm Password</label> 
                <input type="password" name="password2" value="" size="20" maxlength="40" tabindex='3' />
            </div>
        </fieldset>
        <div><input type="submit" name="submit" value="Update Password" tabindex='4' /></div>
    </form>
</div>