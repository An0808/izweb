<div id="content">
    <h2>Retrieve Password</h2>
    <?php 
        if(isset($error)) {
        foreach ($error as $e) {
            echo $e;
        }
    }  
    ?>
    <form id="login" action="" method="post">
        <fieldset>
            <legend>Retrieve Password</legend>
            <div>
                <label for="email">Email: </label> 
                
                <input type="text" name="email" id="email" value="<?php if(isset($_POST['email'])) {echo htmlentities($_POST['email']);} ?>" size="40" maxlength="80" tabindex="1" />
            </div>
        </fieldset>
        <div><input type="submit" name="submit" value="Retrieve Password" /></div>
    </form>
</div><!--end content-->