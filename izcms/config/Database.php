<?php
class Database
{
    // kết nối csdl
    public static $hostname='127.0.0.1';
    public static $username='admin';
    public static $password ='admin';
    public static $dbname='izcms';
    public static $conn ='NULL';

    public function connect(){
        self::$conn=new mysqli(self::$hostname,self::$username,self::$password,self::$dbname);

        if(!self::$conn){
            echo 'Ket Nối Thất Bại';
            
        }
        else
        {
            // echo 'OK';
            mysqli_set_charset(self::$conn,'utf8');
        }
        return self::$conn;

    }
    // câu lệnh chuy vấn 
    public function execute($sql)
    {
        $result= self::$conn->query($sql);
        return $result;
    }
    //get all database 
    public function getAllData($table,$name){
        $result = self::execute("SELECT * FROM $table ORDER BY $name ASC");
           if( mysqli_num_rows($result) > 0)
           {
                while($row = mysqli_fetch_object($result)){
                    $data[]=$row;
                }               
           } 
           else{
               $data =array();
           }
           return $data;
    }

    // Get data theo quan hheej  2 bảng 
    public function getDataQuery($table1,$table2,$id,$name){
        $result = self::execute("SELECT * FROM $table1,$table2 WHERE $table1.$id=$table2.$id ORDER BY $table1.$name ASC ");
        if(mysqli_num_rows($result) >0){
            while($row=mysqli_fetch_object($result)){
                $data[]=$row;
            }
        }
        else{
            $data=array();
        }
        return $data;
    }


// đầu vào cần là 1 bảng table và 1 mảng dữ liệu được người  dùng nhập vào 
//cần 1 chuỗi các biến theo kiểu INSERT INTO $table(key1,key2,key3,...) value (value1,valua2,valua3,..)
    public function addData($table,$mang){
        $str_key= '';
        $str_value ='';
        $array_key = array_flip($mang);
        $key_max= array_pop($array_key);
        // array_pop($mang);
        // print_r($mang);
        // die();

        // vi khi them vao cuoi cung cau mang la 1 string 'now()' ko phai ham now() trong sql 
        if(in_array('now()',$mang)){

            array_pop($mang);
            foreach ($mang as $key => $value){
                //mysqli_real_escape_string lưu dữ liệu trùng với chuy ván sql $mang nó có giá trị là cac post đa có
                $value = mysqli_real_escape_string(self::$conn,strip_tags($value));
                $str_key = $str_key.$key.',';
                $str_value=$str_value."'".$value."',";
            }
            $str_key=rtrim($str_key,',');
            $str_value=rtrim($str_value,',');
            $sql="INSERT INTO $table($str_key, $key_max) VALUES ($str_value,now())";
            return self::execute($sql);
            // echo $sql;
        }
        // add ko co date
        else{
            foreach ($mang as $key => $value){
                //mysqli_real_escape_string lưu dữ liệu trùng với chuy ván sql $mang nó có giá trị là cac post đa có
                $value = mysqli_real_escape_string(self::$conn,strip_tags($value));
                $str_key = $str_key.$key.',';
                $str_value=$str_value."'".$value."',";
            }
            $str_key=rtrim($str_key,',');
            $str_value=rtrim($str_value,',');
            $sql="INSERT INTO $table($str_key) VALUES ($str_value)";
           // echo $sql;
            return self::execute($sql);
            // echo $sql;

        }
        
       
        // return self::execute($sql);
    }


    //đêm số lượng row của 1 bảng với trường lấy ra là tên cot 
    public function count($table,$name){
        $result = self::execute("SELECT COUNT($name) as COUNT FROM $table ");
        if(mysqli_num_rows($result)== 1){
            list($num) =mysqli_fetch_array($result,MYSQLI_NUM);
            //hàm list viet tắt cho $num = $num[0];
        }
        return $num;
    }

    //  lấy data theo  id của table

    public function getDataId($table,$id_table,$id){
        $result = self::execute(" SELECT * FROM $table WHERE $id_table = $id");
        if(mysqli_num_rows($result) > 0){
            while ($row = mysqli_fetch_object($result)){
                $data = $row;
            }
        }
        else {
            $data = 0;
        }
        echo "<br> SELECT * FROM $table WHERE $id_table = $id"."<br>";
        return $data;
    }

    //  Update lại table  update table SET key1='value1',key2='value2'.. Where $id_table = $id;
    public function updateData($table,$mang,$id_table,$id){
        $array_key = array_flip($mang);
        $key_max= array_pop($array_key);
        $result='';

        if(in_array('now()',$mang)){
            array_pop($mang);
            foreach ($mang as $key => $value){
                //mysqli_real_escape_string lưu dữ liệu trùng với chuy ván sql $mang nó có giá trị là cac post đa có
                $value = mysqli_real_escape_string(self::$conn,strip_tags($value));
                $result = $result.$key."='".$value."',"; 
            }
            $result = rtrim($result,',');
            $sql = "UPDATE $table SET $result,$key_max=now() WHERE $id_table = $id LIMIT 1";
            return self::execute($sql);
            // echo $sql;
        }
        else{
            foreach($mang as $key=>$value){
                $value = mysqli_real_escape_string(self::$conn,strip_tags($value));
                $result = $result.$key."='".$value."',"; 
            }
            $result = rtrim($result,',');
            $sql = "UPDATE $table SET $result WHERE $id_table = $id LIMIT 1";
            echo $sql;
            return self::execute($sql);

        }
       

    }

    //Delete data table 

    public function deleteData($table,$id_table,$id){
        $sql="DELETE from $table where $id_table=$id";
        return self::execute($sql);
    }
}
Database::connect();
// $a = new Database;
// echo "<pre>";
// print_r($a->getAllData('customers'));
// echo $a->connect();
?>