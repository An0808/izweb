<?php
    include 'models/user.php'; 
    if($_SERVER['REQUEST_METHOD']=='POST'){
        $error=array();
        if(isset($_POST['email']) && filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)) {
            $email =$_POST['email'];
        }else{
            $error[] ='email';
        }

        if(isset($_POST['password']) && preg_match('/^[\w\.-]{4,20}$/',$_POST['password'])) {
            $password =$_POST['password'];
        }else{
            $error[] ='password';
        }
    
       // print_r($user);
        if(empty($error)){    
            $user = Users::getDataUsersCheck($email,$password) ;
            if($user != 0){
               print_r($user);
                $_SESSION['user_id']=$user->user_id;
                // echo $user->user_id;
                $_SESSION['first_name']=$user->first_name;
                $_SESSION['user_level']=$user->user_level;
                redirect_to();
            }
            else{
                $message="<p class='erorr'>Email or password not match or your have not activated your account<p/>";
            }
        }
        else{
            $message = "<p class='erorr>The fill all the required field</p>";
        }
    }
    require_once 'views/login.php';
?>