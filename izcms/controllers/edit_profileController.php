<?php
    include 'models/user.php';
    if(isset($_POST['upload'])){
       if(isset($_FILES['image'])){
           $errors = array();
           $allowed =array('image/jpeg','image/jpg','image/png','image/x-png');
           print_r($_FILES['image']);
           if(in_array(strtolower($_FILES['image']['type']),$allowed)){
               // nếu có tên file hay ko ,strtolower chuyển tất ả thành viết thường

            $ext =end(explode('.',$_FILES['image']['name']));
            $renamed = uniqid(rand(),true).'.'.$ext;

            if(!move_uploaded_file($_FILES['image']['tmp_name'], "upload/image/".$renamed)){
               echo "<p class='error'>Lỗi Rồi</p>";
            }
            else{
               echo "<p class='error'>ok</p>";
            }
           }
           else{
            $errors[] ="<p class='error'>File ko đúng định giạng</p>";
            }
       }

        if($_FILES['image']['error'] >0){
            // Check for an error
            $errors[] = "<p class='error'>The file could not be uploaded because: <strong>";

            // Print the message based on the error
            switch ($_FILES['image']['error']) {
                case 1:
                    $errors[] .= "The file exceeds the upload_max_filesize setting in php.ini";
                    break;
                    
                case 2:
                    $errors[] .= "The file exceeds the MAX_FILE_SIZE in HTML form";
                    break;
                
                case 3:
                    $errors[] .= "The was partially uploaded";
                    break;
                
                case 4:
                    $errors[] .= "NO file was uploaded";
                    break;

                case 6:
                    $errors[] .= "No temporary folder was available";
                    break;

                case 7:
                    $errors[] .= "Unable to write to the disk";
                    break;

                case 8:
                    $errors[] .= "File upload stopped";
                    break;
                
                default:
                    $errors[] .= "a system error has occured.";
                    break;
            } // END of switch

            $errors[] .= "</strong></p>";
        } // END of error IF

          // Xoa file da duoc upload va ton tai trong thu muc tam
        if(isset($_FILES['image']['tmp_name']) && is_file($_FILES['image']['tmp_name']) && file_exists($_FILES['image']['tmp_name'])) {
            unlink($_FILES['image']['tmp_name']);
        }

        print_r($errors);
        if(empty($errors)){
            echo $renamed;
            if(Users::updateUser(['avatar'=> $renamed],$_SESSION['user_id'])){
                redirect_to('?controller=edit_profile');
            }
        }
    }
    // update data

    if(isset($_POST['edit-user'])){
        $errors = array();
        // Trim all incoming data
        $trimmed = array_map('trim', $_POST);
         
        if(preg_match('/^[\w]{2,10}$/i', $trimmed['first_name'])) {
            $fn = $trimmed['first_name'];
        } else {
            $errors[] = "first_name";
        }
    
        if(preg_match('/^[\w]{2,10}$/i', $trimmed['last_name'])) {
            $ln = $trimmed['last_name'];
        } else {
            $errors[] = "last name";
        }
        
        if(filter_var($trimmed['email'],FILTER_VALIDATE_EMAIL)) {
            $e = $trimmed['email'];
        } else {
            $errors[] = "email";
        }
        // Check for website (not required) 
        $web = (!empty($trimmed['website'])) ? $trimmed['website'] : NULL;
        
        // Check for yahoo (not required) 
        $yahoo = (!empty($trimmed['yahoo'])) ? $trimmed['yahoo'] : NULL;
        
        // Check for website (not required) 
        $bio = (!empty($trimmed['bio'])) ? $trimmed['bio'] : NULL;
        
        if(empty($errors)) {
            Users::updateNew($fn, $ln, $e, $web, $yahoo, $_SESSION['user_id']);
        }
    }
    $dataUsers = Users::getDataUserId($_SESSION['user_id']);
    require_once 'views/edit_profile.php';
?>