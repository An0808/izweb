	<?php
        require_once 'config/Database.php';
        include 'function/function.php';
		include 'includes/header.php';	

		require_once 'controllers/categoriesController.php';

        $controller = null;
		if(isset($_GET['controller'])){
            $controller= $_GET['controller'];
            if($controller == ''){
                include 'views/indexView.php';
            }
            // echo $controller;
        }
        else{
            include 'views/indexView.php';
        }
        switch ($controller) {
            case 'pages':
                require_once 'controllers/pageContent.php';
                break;
            case 'single':
                require_once 'controllers/pageSingle.php';
                break;
            case 'author':
                require_once 'controllers/authorControler.php';
                break;
            case 'contact':
                require_once 'controllers/contactController.php';
                break;
            case 'register':
                require_once 'controllers/registerControler.php';
                break;
            case 'avtivate':
                require_once 'controllers/avtivateController.php';
                break;
            case 'login':
                require_once 'controllers/loginController.php';
                break;
            case 'logout':
                require_once 'controllers/logoutController.php';
                break;
            case 'retrieve_password':
                require_once 'controllers/retrieve_passwordController.php';
                break;
            
            case 'change_password':
                require_once 'controllers/change_passwordController.php';
                break;
            case 'edit_profile':
                require_once 'controllers/edit_profileController.php';
                break;
            // case 'check':
            //         require_once 'controllers/check.php';
            //         break;

           
        }
	
		include 'includes/siderbar-b.php';
		include 'includes/footer.php';
	?>

</body>
</html>